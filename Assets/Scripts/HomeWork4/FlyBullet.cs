using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyBullet : MonoBehaviour
{
    public void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("TargetGun"))
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
        
        
        if (other.gameObject.CompareTag("GranadeTagTarget"))
        {
            Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(GameSettings.power,gameObject.transform.position,GameSettings.GRANADE_EXPLOSION_RADIUS,1.0f);
            }
           Destroy(gameObject, 3f);
        }

    }
}
