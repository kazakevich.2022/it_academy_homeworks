using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UIElements;

public class Bullet : MonoBehaviour
{
    public GameObject bullet;
    public GameObject bulletPrefab;
    public float speed;
    private Vector3 lastPos;
    private Rigidbody rgBullet;
    public Vector3 rotate = new Vector3();
    public Vector3 starBulletPos;

  
    void Start()
    {
        rgBullet = GetComponent<Rigidbody>();
        lastPos = transform.position;
    }

 void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            bullet = Instantiate (bulletPrefab,transform);
           bullet.transform.localPosition =starBulletPos;
           // var bulletRotation = bullet.transform.rotation;
           // bulletRotation.eulerAngles = rotate;
           
            ShootingBullet();
            
        }
       
        
    }

 
 public void ShootingBullet()
    {
        rgBullet = bullet.GetComponent<Rigidbody>();
        rgBullet.AddForce( (bullet.transform.position- transform.position).normalized*speed, ForceMode.Force);
   

       lastPos = transform.position;
    }

  
}



