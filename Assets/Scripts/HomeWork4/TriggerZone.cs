using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZone : MonoBehaviour
{
    public bool isOneTimeTrigger;
    private bool _isTriggered;
    public event Action OnEnterTriggerZone = delegate { };
    public event Action OnExitTriggerZone = delegate { };

    private void OnTriggerEnter(Collider other)
    {
        if (isOneTimeTrigger && _isTriggered)
        {
            return;
        }

        _isTriggered = true;

        if (other.gameObject.CompareTag("Player"))
        {
            print("enter to zone " + gameObject.name);

            OnEnterTriggerZone.Invoke();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (isOneTimeTrigger && _isTriggered)
        {
            return;
        }
        
        _isTriggered = true;
        
        if (other.gameObject.CompareTag("Player"))
        {
            print("enter to zone " + gameObject.name);

            OnExitTriggerZone.Invoke();
        }
    }
}