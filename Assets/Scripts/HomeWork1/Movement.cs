using System;
using UnityEngine;


public class Movement : MonoBehaviour
{
    public GameObject prefab;
    public GameObject instance;
    
    public float Speed;

    public Vector3 Startpos;
    public Vector3 EndPos;

    private bool isEndPosMovement = true;
   // private float accuracy = 0.1f;
    private float lerpCoefficient;
    private float count;
    void Start()
    {
        //transform.position = Startpos;
        instance = Instantiate(prefab, transform);
        instance.transform.position = Startpos;
    }

    // Update is called once per frame
    void Update()
    {
    
         count += Speed * Time.deltaTime;
        lerpCoefficient = Mathf.PingPong(count, 1f);
        instance.transform.position = Vector3.Lerp(Startpos,EndPos,lerpCoefficient);
    }
    
    
    private void OnDestroy()
    {
        if (instance != null)
        {
            Destroy(instance);
        }
    }
}