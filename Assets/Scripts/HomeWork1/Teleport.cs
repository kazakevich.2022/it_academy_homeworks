using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class Teleport : MonoBehaviour
{
    public GameObject prefab;
    private GameObject instance;
    public int delay = 3;

    private float timeCounter;


    private void Update()
    {
        
        if (timeCounter == 0)
        {
            if (prefab == null)
            {
                Debug.LogError(("Prefabs is NULL!"));
            }

            if (instance == null)
            {
                instance = Instantiate(prefab);
            }
            
            var position = new Vector3(Random.Range(-5.0f, 5.0f), 1, Random.Range(-5.0f, 5.0f));
          
            instance.transform.position = position;
        }

        timeCounter += Time.deltaTime;
        if (timeCounter >= delay)
        {
            timeCounter = 0;
        }
    }


    private void Updates()
    {
        
    }
    private void OnDestroy()
    {
        if (instance != null)
        {
            Destroy(instance);
        }
    }
}