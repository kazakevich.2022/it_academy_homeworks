using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DropDownController : MonoBehaviour
{

    [SerializeField] private TMP_Dropdown dropdown;
    [SerializeField] private TMP_Text conclusion;

    private void Start()
    {
        dropdown.onValueChanged.AddListener(OnDropDownChanceOption);
    }

    public void OnDropDownChanceOption(Int32 optionIndex)
    {
        conclusion.text = dropdown.options[optionIndex].text;
        
    }

    private void OnDestroy()
    {
        dropdown.onValueChanged.RemoveListener(OnDropDownChanceOption);
    }
}
