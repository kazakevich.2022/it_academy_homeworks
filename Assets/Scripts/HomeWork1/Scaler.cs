using UnityEngine;

public class Scaler : MonoBehaviour
{
   
    public GameObject prefab;
    private GameObject instantiat;
    
    private Vector3 startScale;
    public Vector3 endScale;
    
    private float lerpCoefficient;
 public float speed =1f;


    void Start()
    {  
        instantiat = Instantiate(prefab);
        startScale = instantiat.transform.localScale;
    }
    
    void Update()
    {
        lerpCoefficient += Time.deltaTime * speed;
        if (lerpCoefficient > 1)
        {
            lerpCoefficient = 1;
        }
        instantiat.transform.localScale=Vector3.Lerp(startScale,endScale, lerpCoefficient);

    }
    
    private void OnDestroy()
    {
        if (instantiat != null)
        {
            Destroy(instantiat);
        }
    }
}
