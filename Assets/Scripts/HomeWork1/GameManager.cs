using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    // public GameObject onePefab;
    // public GameObject twoPrefab;
    // public GameObject treePrefab;
    // public GameObject fourPrefab;
    private GameObject lastActiveMechanic;
    [SerializeField] private List<GameObject> prefabs;
  
    private void Update()
    {

    
        
        if (Input.GetMouseButtonDown(0))
        {
            int indexOfMechanic = Random.Range(0, 4);
            
            if (lastActiveMechanic != null)
            {
                Destroy(lastActiveMechanic);
            }
            switch (indexOfMechanic)
            {
                
                case 0:
                    lastActiveMechanic = Instantiate(prefabs[0]);
                    break;
                case 1:
                    lastActiveMechanic = Instantiate(prefabs[1]);
                    break;
                case 2:
                    lastActiveMechanic = Instantiate(prefabs[2]);
                    break;
                case 3:
                    lastActiveMechanic = Instantiate(prefabs[3]);
                    break;
                
                default:
                    break;
            }
        }
    }
    
    public void CloseApp()
    {
        Application.Quit();
    }
}