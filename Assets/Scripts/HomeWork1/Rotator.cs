using UnityEngine;


public class Rotator : MonoBehaviour
{

    
    public GameObject prefab;
    public Vector3 direction;
    public float angularSpeed;

    private GameObject instantiat;
    
    private void Start()
    {
        instantiat = Instantiate(prefab);
    }

    void Update()
    {
        
        instantiat.transform.Rotate(direction,angularSpeed,Space.World);
        
    }
    
    private void OnDestroy()
    {
        if (instantiat != null)
        {
            Destroy(instantiat);
        }
    }
}
