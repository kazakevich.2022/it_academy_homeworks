using System;
using UnityEditor.Experimental;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{

    private float gravitaci = -9.81f;
    private float jumpSpeed = 7.0f;
    public float speed = 10.0f;
    private float rotateY;
    private float speedY = 10.0f;

    public bool isJump =false;

    public GameObject cameraPlayer;

    private int counter = 10;


    [SerializeField] private CharacterController controller;

    public CharacterController Controller
    {
        get { return controller = controller ?? GetComponent<CharacterController>(); }
    }

  void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float rotateX = Input.GetAxis("Mouse X");

        if (Input.GetKeyDown(KeyCode.Space) && !isJump)
        {
            isJump = true;
            speedY += jumpSpeed;
        }

        if (!controller.isGrounded)
        {
            speedY += gravitaci * Time.deltaTime;
        }
        else if (speedY < 0.0f)
        {
            speedY = 0.0f;
        }

        if (isJump && speedY < 0.0f)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit, 1.5f, LayerMask.GetMask("Default")))
            {
                isJump = false;
            }
        }

        Vector3 movement = new Vector3(horizontal * speed, speedY * Time.deltaTime, vertical * speed);


        controller.Move(transform.TransformDirection(movement));
        controller.transform.Rotate(Vector3.up, rotateX);

        rotateY += Input.GetAxis("Mouse Y");
        rotateY = Mathf.Clamp(rotateY, -30.0f, 15f);
        cameraPlayer.transform.localEulerAngles = new Vector3(-rotateY, 0, 0);
    }
}