using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class ButtonViewMiniCamera : MonoBehaviour
{

    public Button upMiniCamera;
    public Button downMiniCamera;
    public Button faceMiniCamera;
    public Button leftMiniCamera;

    public Transform miniCamera;

    // Start is called before the first frame update
    void Start()
    {
        upMiniCamera.onClick.AddListener(UpViewMiniCamera);
        downMiniCamera.onClick.AddListener(DownViewMiniCamera);
        faceMiniCamera.onClick.AddListener(FaceViewMiniCamera);
        leftMiniCamera.onClick.AddListener(LeftViewMiniCamera);
        
    }

    private void LeftViewMiniCamera()
    {
        miniCamera.transform.localPosition = new Vector3(-16, 0, 0);
        var transformRotation = miniCamera.transform.localRotation;
        transformRotation.eulerAngles =new  Vector3(0,90,0);
        miniCamera.transform.localRotation = transformRotation;
    }

    private void UpViewMiniCamera()
    {
        miniCamera.transform.localPosition = new Vector3(0, 16, 0);
        var transformRotation = miniCamera.transform.localRotation;
        transformRotation.eulerAngles =new  Vector3(90,0,0);
        miniCamera.transform.localRotation = transformRotation;
    }

    public void DownViewMiniCamera()
    {
        
            miniCamera.transform.localPosition = new Vector3(0, -16, 0);
            var transformRotation = miniCamera.transform.localRotation;
            transformRotation.eulerAngles =new  Vector3(-90,0,180);
            miniCamera.transform.localRotation = transformRotation;
        }
    private void FaceViewMiniCamera()
    {
        miniCamera.transform.localPosition = new Vector3(0, 3, 16);
        var transformRotation = miniCamera.transform.localRotation;
        transformRotation.eulerAngles =new  Vector3(180,0,180);
        miniCamera.transform.localRotation = transformRotation;
    }

    private void OnDestroy()
    {
        upMiniCamera.onClick.RemoveListener(UpViewMiniCamera);
        downMiniCamera.onClick.RemoveListener(DownViewMiniCamera);
        faceMiniCamera.onClick.RemoveListener(FaceViewMiniCamera);
        leftMiniCamera.onClick.RemoveListener(LeftViewMiniCamera);
    }
}
