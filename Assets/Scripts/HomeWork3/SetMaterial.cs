using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Renderer))]
public class SetMaterial : MonoBehaviour
{

    public GameObject ship;

    public Button yellowButton;
    public Button blueButton;
    public Button redButton;
    public Button greenButton;

    [SerializeField]  Material redMaterial;
    [SerializeField] Material blueMaterial;
    [SerializeField]  Material greenMaterial;
    [SerializeField]  Material yellowMaterial;


    public bool hasMaterial=true;
    void Start()
    {
        blueButton.onClick.AddListener(ChangeShipColor(blueMaterial));
        redButton.onClick.AddListener(ChangeShipColor(redMaterial));
        greenButton.onClick.AddListener(ChangeShipColor(greenMaterial));
        yellowButton.onClick.AddListener(ChangeShipColor(yellowMaterial));
    }

    // Update is called once per frame
    public UnityAction ChangeShipColor(Material newMaterial)
    {
        return () =>
        {
            if (hasMaterial)
            {
                ship.GetComponent<Renderer>().material = newMaterial;
            }
        };
    }
}
