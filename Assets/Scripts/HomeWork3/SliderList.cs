using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;

public class SliderList : MonoBehaviour
{
    public List<GameObject> listShips;
    public Transform miniCamera;
    public Button nextButton;
    public Button previosButton;
 
    private float speed = 0.5f;

    [SerializeField] private List<Vector3> shipPositions = new List<Vector3>();


    private void Start()
    {
        nextButton.onClick.AddListener(SelectNextShip);
        previosButton.onClick.AddListener(SelectPreviousShip);
        UpdateAbilityRotateShips();
        UpdateMiniCamera();
        UpdateAbiliteSetmaterial();
        
    }

    private void UpdateMiniCamera()
    {
        miniCamera.SetParent(listShips[0].transform,false);
       
    }


    private void SelectNextShip()
    {
        for (int i = 0; i < listShips.Count; i++)
        {
            if (i == 0)
            {
                StartCoroutine(MoveCharacterToPos(listShips[i], shipPositions.Last()));
                StartCoroutine(DelayedCall(1f, () =>
                {
                    foreach (var ship in listShips)
                    {
                        ship.transform.rotation = new Quaternion();
                    }
                }));
            }
            else
            {
                StartCoroutine(MoveCharacterToPos(listShips[i], shipPositions[i - 1]));
            }
        }

        var firstShip = listShips[0];
        listShips.Add(firstShip);
        listShips.RemoveAt(0);
        
        UpdateAbilityRotateShips();
        UpdateMiniCamera();
        UpdateAbiliteSetmaterial();
    }



    private void SelectPreviousShip()
    {
        for (int i = 0; i < listShips.Count; i++)
        {
            if (i == listShips.Count - 1)
            {
                StartCoroutine(MoveCharacterToPos(listShips[i], shipPositions[0]));
            }
            else
            {
                StartCoroutine(MoveCharacterToPos(listShips[i], shipPositions[i + 1]));
                StartCoroutine(DelayedCall(1, () =>
                {
                    foreach (var ship in listShips)
                    {
                        ship.transform.rotation = new Quaternion();
                    }
                }));
            }
        }
        
        listShips.Insert(0,listShips.Last());
        listShips.RemoveAt(listShips.Count-1);
        
       
        UpdateMiniCamera();
        UpdateAbilityRotateShips();
        UpdateAbiliteSetmaterial();
    }
    private void UpdateAbiliteSetmaterial()
    {
        listShips[0].GetComponent<SetMaterial>().hasMaterial = true;
        for (int i = 0; i < listShips.Count; i++)
        {
            if (i >= 1)
            {
                listShips[i].GetComponent<SetMaterial>().hasMaterial = false;
            }
        }
    }

    private void UpdateAbilityRotateShips()
    {
        listShips[0].GetComponent<RotateShip>().hasShip = true;
        for (int i = 0; i < listShips.Count; i++)
        {
            if (i >= 1)
            {
                listShips[i].GetComponent<RotateShip>().hasShip = false;
            }
        }
    }
    private IEnumerator DelayedCall(float delay, Action actionAfterDelay)
    {
        yield return new WaitForSeconds(delay);
        actionAfterDelay?.Invoke();
    }

    private IEnumerator MoveCharacterToPos(GameObject obj, Vector3 targetPos, float duration = 1f,
        float startAwaitTime = 0f)
    {
        yield return new WaitForSeconds(startAwaitTime);
        var moveProgressCoeff = 0f;
        var startCharacterPos = obj.transform.localPosition;

        while (moveProgressCoeff <= 1)
        {
            moveProgressCoeff += Time.deltaTime / duration;
            obj.transform.localPosition = Vector3.Lerp(startCharacterPos, targetPos, moveProgressCoeff);
            yield return null;
        }
    }
}