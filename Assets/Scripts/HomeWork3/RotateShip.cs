using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RotateShip : MonoBehaviour
{
    
    public float moveSpeed = 10;
    public bool hasShip = true;
   Vector3 lastMousePosition = Vector3.zero;
   [SerializeField] private Button rotateButton;
    void Update()
    {

        if (EventSystem.current.currentSelectedGameObject==rotateButton.gameObject)
        {
            if (Input.GetMouseButton(0) && hasShip)
            {
                if (lastMousePosition == Vector3.zero)
                {
                    lastMousePosition = Input.mousePosition;
                }
                else
                {
                    var rotateDistance = (Input.mousePosition - lastMousePosition)*(moveSpeed * Time.deltaTime);
                   // transform.Rotate(rotateDistance);
                  // var rotY = rotateDistance.y;
                   var rotX = rotateDistance.x;
                    transform.Rotate(new Vector3(0, -rotX, 0.0f));
                    
                    lastMousePosition = Input.mousePosition;
                }
            }
        }
        
      

    }
}
