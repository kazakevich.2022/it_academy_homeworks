using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
public class RespawnRoom : MonoBehaviour
{
    public Transform player;
    public Room[] roomPrefab;
    public Room firstRoom;


    [SerializeField] private List<Room> listRooms = new List<Room>();

  
    
    void Start()
    {
        listRooms.Add(firstRoom);
        SpawnUpRooms();
        
        listRooms[0].upTriggerRoom.OnEnterTriggerZone += () =>
        {
            SpawnUpRooms();
        };
   
    }
    
    private void SpawnUpRooms()
    {
        
        Room newRoom = Instantiate(roomPrefab[Random.Range(0,roomPrefab.Length)]);
        newRoom.upTriggerRoom.OnEnterTriggerZone += SpawnUpRooms;
        listRooms.Add(newRoom);
      
        listRooms[listRooms.Count-2].upNeighbor = listRooms[listRooms.Count-1];
        listRooms[listRooms.Count-1].downNeighbor = listRooms[listRooms.Count-2];
        
        newRoom.transform.position = listRooms[listRooms.Count -2].Begin.position;
        //newRoom.transform.localPosition = new Vector3(0, newRoom.transform.localPosition.y, 0);
        
        // if (listRooms.Count >= 5)
        // {
        //     Destroy(listRooms[0].gameObject);
        //     listRooms.RemoveAt(0);
        // }
    } 

    private void SpawnDownRooms()
    {
        // Room newRoom = Instantiate(roomPrefab[Random.Range(0,roomPrefab.Length)]);
        // listRooms.Add(newRoom);
        // if (listRooms.Count > 2)
        // {
        //     listRooms.Sort();
        //     
        // }
        // listRooms.Reverse();
        // listRooms[listRooms.Count -2].downNeighbor = listRooms[listRooms.Count-1];
        // listRooms[listRooms.Count-1].upNeighbor = listRooms[listRooms.Count - 2];
        // newRoom.transform.position = listRooms[listRooms.Count -1].End.position;
        //newRoom.transform.localPosition = new Vector3(0, newRoom.transform.localPosition.y, 0);
       
    }
   
}

