using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour, IComparable<Room>
{
    public Transform Begin;
    public Transform End;
    public Room upNeighbor;
    public Room downNeighbor;

    public TriggerZone upTriggerRoom;
    public TriggerZone downTriggerRoom;
    
    public int CompareTo(Room other)
    {
        if (ReferenceEquals(this, other)) return 0;
        if (ReferenceEquals(null, other)) return 1;
        var gameObjectYPositionComparison = (int)(transform.position.y - other.transform.position.y);
        return gameObjectYPositionComparison;
    }
}
