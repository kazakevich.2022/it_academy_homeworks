using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementElle : MonoBehaviour
{
    public float movementSpeed = 20.0f;
    public float sprintSpeed = 5.0f;
    public float rotationSpeed = 0.2f;
    public float animationBlendSpeed = 0.2f;
    public float jumpSpeed = 5.0f;
    private float rotationAngle = 0.0f;
    private float targetAnimationSpeed = 0.0f;
    
    private float speedY = 0.0f;
    private float gravity = -9.81f;
    public bool isJump = true;
    private bool isSprint = false;
    
    public CharacterController _controller;
    private Animator _animator;
    private Camera characterCamera;
    
    public CharacterController Controller { get { return _controller=_controller ?? GetComponent<CharacterController>(); } }
    public Camera CharacterCamera { get { return characterCamera=characterCamera ?? FindObjectOfType<Camera>(); } }
    public Animator CharacterAnimator { get { return _animator = _animator ?? GetComponent<Animator>(); } }
    


    void Update()
    {
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        if (Input.GetKeyDown(KeyCode.K))
        {
            CharacterAnimator.Play("EllenDeath");
            CharacterAnimator.SetTrigger("Death");
        }
       
        JumpLogic();

       
        Vector3 move = new Vector3(horizontal*Time.deltaTime, 0.0f, vertical*Time.deltaTime);
        Vector3 rotatePlayer = Quaternion.Euler(0, CharacterCamera.transform.rotation.eulerAngles.y, 0) * move.normalized;
        Vector3 verticalMove = Vector3.up*speedY;
        
        
        isSprint = Input.GetKeyDown(KeyCode.LeftShift);
       // float currentSpeed = isSprint ? sprintSpeed : movementSpeed;
        float currentSpeed = isSprint ? movementSpeed : sprintSpeed;
        
        Controller.SimpleMove((verticalMove + rotatePlayer * currentSpeed) * Time.deltaTime);

        if (rotatePlayer.sqrMagnitude > 0.0f)
        {
            rotationAngle = Mathf.Atan2(rotatePlayer.x, rotatePlayer.z)*Mathf.Rad2Deg;
            targetAnimationSpeed = isSprint ? 1.0f : 0.5f;
            if (!isSprint)
            {
                CharacterAnimator.Play("Walk");
            }

            isSprint = true;
           }
        else
        {
            if (isSprint)
            {
                CharacterAnimator.Play("Run");
            }
            targetAnimationSpeed = 0.0f;
        }
        
        CharacterAnimator.SetFloat("Speed",Mathf.Lerp(CharacterAnimator.GetFloat("Speed"), targetAnimationSpeed, animationBlendSpeed*Time.deltaTime));
        Quaternion currentRotation = Controller.transform.rotation;
        Quaternion targetRotation = Quaternion.Euler(0.0f, rotationAngle,0.0f);
        Controller.transform.rotation = Quaternion.Lerp(currentRotation,targetRotation,rotationSpeed);

    }

    private void JumpLogic()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !isJump)
        {
            isJump = true;
            CharacterAnimator.Play("Jumping");
            CharacterAnimator.SetTrigger("Jump");
            CharacterAnimator.ResetTrigger("Land");
            speedY += jumpSpeed;
        }

        if (!_controller.isGrounded)
        {
            speedY += gravity * Time.deltaTime;
        }
        else if (speedY < 0.0f)
        {
            speedY = 0.0f;
        }

        CharacterAnimator.SetFloat("speedY", Mathf.Abs(speedY / jumpSpeed));
        if (isJump && speedY < 1.0f)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit, 0.5f, LayerMask.GetMask("Land")))
            {
                CharacterAnimator.SetTrigger("Land");
                CharacterAnimator.ResetTrigger("Jump");
            }

            isJump = false;
        }
    }
   
}
