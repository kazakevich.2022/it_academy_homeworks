using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEditor.Animations;
using UnityEngine;
using Random = UnityEngine.Random;

public class AnimatorElle : MonoBehaviour
{
    private static readonly int SpeedY = Animator.StringToHash("SpeedY");
    private float speedY = 0.0f;

    public float increasingSpeedCoefficient = 0.1f;
    public float decreasingSpeedCoefficient = 0.1f;
    
    private bool _isPlayingAttackAnimation = false;
    public float movementSpeed = 20.0f;
    public float sprintSpeed = 5.0f;
    public float rotationSpeed = 0.2f;
    public float animationBlendSpeed = 0;
    private float rotationAngle = 0.0f;
    private float targetAnimationSpeed = 0.0f;
    
    private float gravity = -9.81f;

    [SerializeField] private Animator anim;
    public CharacterController _controller;
    [SerializeField] private float jumpDuration = 3f;
    private Camera characterCamera;
    private int _combo1Hash; 
    private int _combo2Hash; 
    private int _combo3Hash; 
    private int _combo4Hash;
    public Camera CharacterCamera
    {
        get { return characterCamera = characterCamera ?? FindObjectOfType<Camera>(); }
    }

    public Animator CharacterAnimator
    {
        get { return anim = anim ?? GetComponent<Animator>(); }
    }

    public CharacterController Controller
    {
        get { return _controller = _controller ?? GetComponent<CharacterController>(); }
    }

    private Coroutine _jumpRoutine;
    private bool _isDead = false;

    private void Start()
    {
        _combo1Hash = Animator.StringToHash("Combo1");
        _combo2Hash = Animator.StringToHash("Combo2");
        _combo3Hash = Animator.StringToHash("Combo3");
        _combo4Hash = Animator.StringToHash("Combo4");
    }

    void Update()
    {
        if (!_isDead)
        {
            DeathLogic();
            MoveAndRunLogic();
            JumpLogic();
            RandomAttack();
        }
        else
        {
            RespawnLogic();
        }
    }

    private void RespawnLogic()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            CharacterAnimator.CrossFade("Spawn",0f);
            _isDead = false;
        }
    }

    private void JumpLogic()
    {
        if (Input.GetKey(KeyCode.Space) && _jumpRoutine == null)
        {
            _jumpRoutine = StartCoroutine(JumpRoutine());
        }
    }

    private void MoveAndRunLogic()
    {
        if (_jumpRoutine != null && _isPlayingAttackAnimation == false)
        {
            return;
        }

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) ||
            Input.GetKey(KeyCode.D))
        {
            animationBlendSpeed += Time.deltaTime* increasingSpeedCoefficient ;
        }
        else
        {
            animationBlendSpeed -= Time.deltaTime * decreasingSpeedCoefficient;
        }

        animationBlendSpeed = Mathf.Clamp01(animationBlendSpeed);
      
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");
        Vector3 move = new Vector3(horizontal * Time.deltaTime, 0.0f, vertical * Time.deltaTime);
        Vector3 rotatePlayer = Quaternion.Euler(0, CharacterCamera.transform.rotation.eulerAngles.y, 0) * move.normalized;
        Vector3 verticalMove = Vector3.up * speedY;
        
        float currentSpeed = Input.GetKey(KeyCode.LeftShift) ? sprintSpeed : movementSpeed;
        targetAnimationSpeed =  Input.GetKey(KeyCode.LeftShift) ? 1.0f : 0.5f;
        Controller.SimpleMove((verticalMove + rotatePlayer * currentSpeed) * Time.deltaTime);

        if (rotatePlayer.sqrMagnitude > 0.0f)
        {
            rotationAngle = Mathf.Atan2(rotatePlayer.x, rotatePlayer.z) * Mathf.Rad2Deg;
        }
        else
        {
            targetAnimationSpeed = 0.0f;
        }
        
        CharacterAnimator.SetFloat("Speed",
            Mathf.Lerp(0, targetAnimationSpeed, animationBlendSpeed));
        Quaternion currentRotation = Controller.transform.rotation;
        Quaternion targetRotation = Quaternion.Euler(0.0f, rotationAngle, 0.0f);
        Controller.transform.rotation = Quaternion.Lerp(currentRotation, targetRotation, rotationSpeed);
    }

    private void DeathLogic()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            CharacterAnimator.Play("EllenDeath");
        }
    }

    private IEnumerator JumpRoutine()
    {
        CharacterAnimator.ResetTrigger("Land");
        CharacterAnimator.SetFloat(SpeedY, 0);
        CharacterAnimator.CrossFade("Jumping", 0.1f);

        var progressCoefficient = 0f;
        while (progressCoefficient < 1f)
        {
            progressCoefficient += Time.deltaTime / jumpDuration;
            if (progressCoefficient > 1f)
            {
                progressCoefficient = 1f;
            }

            speedY = Mathf.Lerp(0.0f, 1.0f, progressCoefficient);
            CharacterAnimator.SetFloat(SpeedY, speedY);
            yield return null;
        }

        CharacterAnimator.SetTrigger("Land");

        _jumpRoutine = null;
    }

    private  async void RandomAttack ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _isPlayingAttackAnimation = true;
            int indexKick = Random.Range(0, 4);
            switch (indexKick)
            {
                case 0:
                    CharacterAnimator.CrossFade(_combo1Hash,0); 
                    await Task.Delay(4);    // небольшая задержка для убеждения смены состояния аниматором
                    while (CharacterAnimator.GetCurrentAnimatorStateInfo(0).shortNameHash == _combo1Hash)
                    {
                        await Task.Delay(4);    
                    }
                    _isPlayingAttackAnimation = false;
                    break;
                case 1:
                    CharacterAnimator.CrossFade(_combo2Hash,0); 
                    await Task.Delay(4);    
                    while (CharacterAnimator.GetCurrentAnimatorStateInfo(0).shortNameHash == _combo2Hash)
                    {
                        await Task.Delay(4);    
                    }
                    _isPlayingAttackAnimation = false;
                    break;
                case 2:
                    CharacterAnimator.CrossFade(_combo3Hash,0); 
                    await Task.Delay(4);    
                    while (CharacterAnimator.GetCurrentAnimatorStateInfo(0).shortNameHash == _combo3Hash)
                    {
                        await Task.Delay(4);    
                    }
                    _isPlayingAttackAnimation = false;
                    break;
                case 3:
                    CharacterAnimator.CrossFade(_combo4Hash, 0); 
                    await Task.Delay(4);    
                    while (CharacterAnimator.GetCurrentAnimatorStateInfo(0).shortNameHash == _combo4Hash)
                    {
                        await Task.Delay(4);    
                    }
                    _isPlayingAttackAnimation = false;
                    break;
            }

            
            
            
        }

       
    }
  
    
    
}